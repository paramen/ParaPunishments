package fr.comore.paramen.punishments;

import fr.comore.paramen.punishments.commands.BanCommand;
import fr.comore.paramen.punishments.commands.KickCommand;
import fr.comore.paramen.punishments.commands.MuteCommand;
import fr.comore.paramen.punishments.listeners.PunishmentsListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class ParaPunishments extends JavaPlugin {

    @Override
    public void onEnable() {
        getCommand("ban").setExecutor(new BanCommand());
        getCommand("unban").setExecutor(new BanCommand());
        getCommand("mute").setExecutor(new MuteCommand());
        getCommand("unmute").setExecutor(new MuteCommand());
        getCommand("kick").setExecutor(new KickCommand());
        Bukkit.getPluginManager().registerEvents(new PunishmentsListener(), this);
        super.onEnable();
    }
}
