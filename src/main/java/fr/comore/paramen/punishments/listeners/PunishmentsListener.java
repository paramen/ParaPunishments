package fr.comore.paramen.punishments.listeners;

import fr.comore.paramen.punishments.punishmentManager.BanManager;
import fr.comore.paramen.punishments.punishmentManager.MuteManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import java.util.UUID;

public class PunishmentsListener implements Listener {

    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        Player target = event.getPlayer();

        new BanManager().checkDuration(target.getUniqueId());
        if(new BanManager().isBanned(target.getUniqueId())) {

            event.setResult(PlayerLoginEvent.Result.KICK_BANNED);
            if(new BanManager().getEnd(target.getUniqueId()) == -1) {
                event.setKickMessage("§cYou are permanently banned from Paramen.\n§7Il you feel this ban is unjustified, appeal on our TeamSpeak.\n§6You may also purchase an unban at store.paramen.eu.");
            } else {
                event.setKickMessage("§cYou are temporarily banned from Paramen.\n§7Il you feel this ban is unjustified, appeal on our TeamSpeak.\n§6You may also purchase an unban at store.paramen.eu.");
            }
        }
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player target = event.getPlayer();
        UUID uuid = target.getUniqueId();

        new MuteManager().checkDuration(uuid);
        if(new MuteManager().isMuted(uuid)) {
            event.setCancelled(true);
            if(new MuteManager().getEnd(uuid) == -1) {
                target.sendMessage("§eYou're permanently muted §efor '"+new MuteManager().getReason(uuid)+"§e'.");
            } else {
                target.sendMessage("§eYou're temporarily muted ("+new MuteManager().getTimeLeft(uuid)+"§e) §efor '"+new MuteManager().getReason(uuid)+"§e'.");
            }
        }
    }

}
