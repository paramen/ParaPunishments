package fr.comore.paramen.punishments.commands;

import fr.comore.paramen.punishments.punishmentManager.BanManager;
import fr.comore.paramen.punishments.time.TimeUnit;
import fr.comore.paramen.rank.accounts.Account;
import fr.comore.paramen.rank.ranks.RankUnit;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class KickCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(label.equalsIgnoreCase("kick")){
            if(sender instanceof Player && Account.getAccount((Player) sender).getRank().getPower() < RankUnit.TRIALMOD.getPower()){
                sender.sendMessage("§eYou don't have necessary permissions to perform this command.");
                return false;
            }

            if(args.length < 2){
                helpMessage(sender);
                return false;
            }

            String targetName = args[0];

            String reason = "";
            for(int i = 1; i < args.length; i++){
                reason += args[i] + " ";
            }

            if(Bukkit.getPlayer(targetName) == null) {
                sender.sendMessage("§eThis player is not online.");
                return false;
            }
            sender.sendMessage("§e"+Bukkit.getPlayer(targetName).getName() + " was kicked from the server.");
            Bukkit.getPlayer(targetName).kickPlayer("§cYou are kicked from Paramen.\n§7If you feel this kick is unjustified, appeal on our TeamSpeak.");
            return false;
        }
        return false;
    }

    private void helpMessage(CommandSender sender){
        sender.sendMessage("§eUsage: /kick <player> <reason>");
    }
}
