package fr.comore.paramen.punishments.commands;

import fr.comore.paramen.punishments.punishmentManager.BanManager;
import fr.comore.paramen.punishments.time.TimeUnit;
import fr.comore.paramen.rank.accounts.Account;
import fr.comore.paramen.rank.ranks.RankUnit;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class BanCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(label.equalsIgnoreCase("ban")){
            if(sender instanceof Player && Account.getAccount((Player) sender).getRank().getPower() < RankUnit.MOD.getPower()){
                sender.sendMessage("§eYou don't have necessary permissions to perform this command.");
                return false;
            }

            if(args.length < 3){
                helpMessage(sender);
                return false;
            }

            String targetName = args[0];

            if(!(Bukkit.getOfflinePlayer(targetName).hasPlayedBefore())){
                sender.sendMessage("§eThis player hasn't played before on the server.");
                return false;
            }

            UUID targetUUID = Bukkit.getOfflinePlayer(targetName).getUniqueId();

            if(new BanManager().isBanned(targetUUID)){
                sender.sendMessage("§eThis player is already banned !");
                return false;
            }

            String reason = "";
            for(int i = 2; i < args.length; i++){
                reason += args[i] + " ";
            }

            if(args[1].equalsIgnoreCase("PERMA")){
                new BanManager().ban(targetUUID, -1, reason);
                sender.sendMessage("§e"+Bukkit.getOfflinePlayer(targetUUID).getName() + " was permanently banned for " + sender.getName());
                return false;
            }

            if(!args[1].contains(":")){
                helpMessage(sender);
                return false;
            }

            int duration = 0;
            try {
                duration = Integer.parseInt(args[1].split(":")[0]);
            } catch(NumberFormatException e){
                sender.sendMessage("§eError: Incorrect value.");
                return false;
            }

            if(!TimeUnit.existFromShortcut(args[1].split(":")[1])){
                sender.sendMessage("§eIncorrect time unit !");
                for(TimeUnit units : TimeUnit.values()){
                    sender.sendMessage("§e" + units.getName() + " §f: §e" + units.getShortcut());
                }
                return false;
            }

            TimeUnit unit = TimeUnit.getFromShortcut(args[1].split(":")[1]);
            long banTime = unit.getToSecond() * duration;

            new BanManager().ban(targetUUID, banTime, reason);
            sender.sendMessage("§e"+Bukkit.getOfflinePlayer(targetUUID).getName() + " was temporarily banned for " + sender.getName());
            return false;
        }

        if(label.equalsIgnoreCase("unban")){
            if(sender instanceof Player && Account.getAccount((Player) sender).getRank().getPower() < RankUnit.SRMOD.getPower()){
                sender.sendMessage("§eYou don't have necessary permissions to perform this command.");
                return false;
            }

            if(args.length != 1){
                sender.sendMessage("§eUsage: /unban <player>");
                return false;
            }

            String targetName = args[0];

            if(!(Bukkit.getOfflinePlayer(targetName).hasPlayedBefore())){
                sender.sendMessage("§eThis player hasn't played before on the server.");
                return false;
            }

            UUID targetUUID = Bukkit.getOfflinePlayer(targetName).getUniqueId();

            if(!new BanManager().isBanned(targetUUID)){
                sender.sendMessage("§eThis player is not banned.");
                return false;
            }

            new BanManager().unban(targetUUID);
            sender.sendMessage("§6You've unbanned " + targetName);
            return false;
        }
        return false;
    }

    private void helpMessage(CommandSender sender){
        sender.sendMessage("§eUsage: /ban <player> PERMA <reason>");
        sender.sendMessage("§eUsage: /ban <player> <time>:<unit> <reason>");
    }
}
