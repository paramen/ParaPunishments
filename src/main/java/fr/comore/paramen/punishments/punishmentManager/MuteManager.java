package fr.comore.paramen.punishments.punishmentManager;


import fr.comore.paramen.punishments.time.TimeUnit;
import fr.comore.paramen.rank.ParaRank;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.UUID;

public class MuteManager {

    public void mute(UUID uuid, long endInSeconds, String reason){
        if(isMuted(uuid)) return;

        long endToMillis = endInSeconds * 1000;
        long end = endToMillis + System.currentTimeMillis();

        if(endInSeconds == -1){
            end = -1;
        }

        ParaRank.getInstance().getMySQL().update("INSERT INTO mutes (player_uuid, end, reason) VALUES ('"+uuid.toString()+"', '"+end+"', '"+reason+"')");


        if(Bukkit.getPlayer(uuid) != null){
            Player target = Bukkit.getPlayer(uuid);
            if(getEnd(uuid) == -1) {
                target.sendMessage("§eYou're permanently muted §efor "+getReason(uuid)+"");
            } else {
                target.sendMessage("§eYou're temporarily muted ("+getTimeLeft(uuid)+"§e) §efor "+getReason(uuid)+"");
            }
        }
    }

    public void unmute(UUID uuid){
        if(!isMuted(uuid)) return;

        ParaRank.getInstance().getMySQL().update("DELETE FROM mutes WHERE player_uuid='"+uuid.toString()+"'");
    }

    public boolean isMuted(UUID uuid){
        return (boolean) ParaRank.getInstance().getMySQL().query("SELECT * FROM mutes WHERE player_uuid='"+uuid.toString()+"'", resultSet -> {

            try {
                return resultSet.next();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return false;
        });
    }

    public void checkDuration(UUID uuid){
        if(!isMuted(uuid)) return;

        if(getEnd(uuid) == -1) return;

        if(getEnd(uuid) < System.currentTimeMillis()){
            unmute(uuid);
        }
    }

    public long getEnd(UUID uuid){
        if(!isMuted(uuid)) return 0;

        return (long) ParaRank.getInstance().getMySQL().query("SELECT * FROM mutes WHERE player_uuid='"+uuid.toString()+"'", resultSet -> {

            try {
                if(resultSet.next()) {
                    return resultSet.getLong("end");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return (long) 0;
        });
    }

    public String getTimeLeft(UUID uuid){
        if(!isMuted(uuid)) return "§cNon muted";

        if(getEnd(uuid) == -1){
            return "§cPermanent";
        }

        long tempsRestant = (getEnd(uuid) - System.currentTimeMillis()) / 1000;
        int Month = 0;
        int DAY = 0;
        int Hours = 0;
        int minutes = 0;
        int secondes = 0;

        while(tempsRestant >= TimeUnit.MONTH.getToSecond()){
            Month++;
            tempsRestant -= TimeUnit.MONTH.getToSecond();
        }

        while(tempsRestant >= TimeUnit.DAY.getToSecond()){
            DAY++;
            tempsRestant -= TimeUnit.DAY.getToSecond();
        }

        while(tempsRestant >= TimeUnit.HOUR.getToSecond()){
            Hours++;
            tempsRestant -= TimeUnit.HOUR.getToSecond();
        }

        while(tempsRestant >= TimeUnit.MINUTE.getToSecond()){
            minutes++;
            tempsRestant -= TimeUnit.MINUTE.getToSecond();
        }

        while(tempsRestant >= TimeUnit.SECOND.getToSecond()){
            secondes++;
            tempsRestant -= TimeUnit.SECOND.getToSecond();
        }

        // 1 Month, 1 DAYs), 12 Hour(s), 32 Minute(s), 12 Seconde(s)
        return (Month == 0 ? "" : Month + " " + TimeUnit.MONTH.getName() + ", ") + (DAY == 0 ? "" : DAY + " " + TimeUnit.DAY.getName() + ", ") + (Hours == 0 ? "" : Hours + " " + TimeUnit.HOUR.getName() + ", ") + (minutes == 0 ? "" : minutes + " " + TimeUnit.MINUTE.getName() + ", ") + (secondes == 0 ? "" : secondes + " " + TimeUnit.SECOND.getName());
    }

    public String getReason(UUID uuid){
        if(!isMuted(uuid)) return "§cNon muted";

        return (String) ParaRank.getInstance().getMySQL().query("SELECT * FROM mutes WHERE player_uuid='"+uuid.toString()+"'", resultSet -> {
            try {
                if(resultSet.next()) {
                    return resultSet.getString("reason");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return "";
        });
    }

}