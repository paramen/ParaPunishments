package fr.comore.paramen.punishments.time;

import java.util.HashMap;

public enum TimeUnit {

    SECOND("second(s)", "sec", 1),
    MINUTE("minute(s)", "min", 60),
    HOUR("hour(s)", "h", 60 * 60),
    DAY("day(s)", "d", 60 * 60 * 24),
    MONTH("month(s)", "m", 60 * 60 * 24 * 30);

    private String name;
    private String shortcut;
    private long toSecond;

    private static HashMap<String, TimeUnit> id_shortcuts = new HashMap<String, TimeUnit>();

    TimeUnit(String name, String shortcut, long toSecond) {
        this.name = name;
        this.shortcut = shortcut;
        this.toSecond = toSecond;
    }

    static {
        for(TimeUnit units : values()){
            id_shortcuts.put(units.shortcut, units);
        }
    }

    /**
     * Récupérer le TimeUnit associé au shortcut
     * @param shortcut
     * @return TimeUnit
     */
    public static TimeUnit getFromShortcut(String shortcut){
        return id_shortcuts.get(shortcut);
    }

    public String getName(){
        return name;
    }

    public String getShortcut(){
        return shortcut;
    }

    public long getToSecond() {
        return toSecond;
    }

    public static boolean existFromShortcut(String shortcut){
        return id_shortcuts.containsKey(shortcut);
    }

}